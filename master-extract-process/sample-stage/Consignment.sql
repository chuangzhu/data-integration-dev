﻿CREATE TABLE [dbo].[Consignment] (
    [ConsignmentNo]         NVARCHAR (50)  NOT NULL,
    [ConsignmentItems]      NVARCHAR (255) NULL,
    [ItemCount]             INT           NULL,
    [PostCode]              NVARCHAR (25)  NULL,
    [ServiceProvideDepot]   NVARCHAR (50)  NULL,
    [Status]                NVARCHAR (50)  NULL,
    [StatusAvilabilityDate] DATETIME      NULL,
    [StatusCaptureDate]     DATETIME      NULL,
    [Suburb]                NVARCHAR (50) NULL,
    CONSTRAINT [PK_Consignment] PRIMARY KEY CLUSTERED ([ConsignmentNo] ASC)
);


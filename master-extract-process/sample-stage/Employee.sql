﻿CREATE TABLE [dbo].[Employee] (
    [EmployeeID]   INT           NOT NULL,
    [EmployeeName] NVARCHAR (150) NULL,
    [EmployeeType] NVARCHAR (50)  NULL,
    [PostCode]     NVARCHAR (50)  NULL,
    [State]        NVARCHAR (50)  NULL,
    [TeamCode]     NVARCHAR (50)  NULL,
	[CatCode]	NVARCHAR(50) NULL,
    CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([EmployeeID] ASC)
);


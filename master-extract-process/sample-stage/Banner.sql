﻿CREATE TABLE [dbo].[Banner] (
    [BannerCode]       NVARCHAR(50)  NOT NULL,
    [BannerName]       NVARCHAR(100) NULL,
    [ParentBannerCode] NVARCHAR(50)  NULL,
    [ParentBannerName] NVARCHAR(100) NULL,
    CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED ([BannerCode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Banner]
    ON [dbo].[Banner]([BannerName] ASC);


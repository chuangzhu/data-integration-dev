﻿CREATE TABLE [dbo].[Customer] (
    [BannerCode]     NVARCHAR (10) NULL,
    [BusinessUnitId] NVARCHAR (50)   NULL,
    [CatCode09]      NVARCHAR (50)   NULL,
    [CatCode20]      NVARCHAR (50)   NULL,
    [Channel]        NVARCHAR (100)  NULL,
    [City]           NVARCHAR (100)  NULL,
    [CustomerId]     NVARCHAR (50)   NOT NULL,
    [CustomerName]   NVARCHAR (255) NOT NULL,
    [DemandGroup]    NVARCHAR (50)   NULL,
    [PostCode]       NVARCHAR (50)   NULL,
    [SalesUnit]      NVARCHAR (50)   NULL,
    [SellingRegion]  NVARCHAR (50)   NULL,
    [ShopperChannel] NVARCHAR (50)   NULL,
    [State]          NVARCHAR (50)   NULL,
    [SubChannel]     NVARCHAR (50)   NULL,
    [Type]           NVARCHAR (3)    NULL,
    CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([CustomerId] ASC)
);

